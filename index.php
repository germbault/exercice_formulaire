<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intro PHP</title>
</head>
<body>

<?php

    //Je peux stocker les valeurs de mon formulaire   
    $prenom = (isset($_GET['prenom']) ? $_GET['prenom'] : ""); 
    $nom = (isset($_GET['nom']) ? $_GET['nom'] : ""); 

    // Ici je fait différente section entre le php et le texte
    if(isset($_GET['prenom']) && isset($_GET['nom'])){
    ?>
    <h2> Bienvenu 
    <?php
        echo $_GET['prenom']. $_GET['nom'];
    ?>
    </h2>
    <?php
    }

    /*
        Ici je ne fait que trois echo, n'étant que tdu texte interprété par le navigateur 
        je peux ecrire du html dans mes chaines de caractères. 
    */
    if(isset($_GET['prenom']) && isset($_GET['nom'])){
        echo "<h2> Bienvenu "; 
        echo $_GET['prenom']. $_GET['nom'];
        echo "</h2>";
    }

    /*
        Ici je fait la concatenation des valeurs de mon formulaire
        Pour les afficher
    */ 
    if(isset($_GET['prenom']) && isset($_GET['nom'])){
        echo "<h2> Bienvenu ". $_GET['prenom']. $_GET['nom'] ."</h2>";
    }

    /* 
        Ici j'utilise les "" pour pouvoir interpoler automatiquement 
        les variable dans ma chaine de caractères.
    */
    if(isset($_GET['prenom']) && isset($_GET['nom'])){
        echo "<h2> Bienvenu $prenom $nom </h2>";
    } 
    /* 
        Toutes ces méthodes sont bonnes mais je préfère la dernière 
        Court clair concis efficace. Mais elle n'est pas toujours la méthode la 
        plus appropriée pour générer du html. Selon le cas différentes méthodes 
        seront utilisées. 
    */

?>

<form action="/" method="GET" >
    <input type="text" name="prenom"  placeholder="Prénom"/><br />
    <input type="text" name="nom" placeholder="Nom" /><br />
    <input type="submit" value="Soumettre"/>
</form>

</body>
</html>